FROM debian:9 as build
RUN apt update && apt install wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev -y
RUN wget http://nginx.org/download/nginx-1.19.8.tar.gz                                   && \
    wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20201229.tar.gz     && \
    wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz        && \
    wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz

RUN tar zxvf nginx-1.19.8.tar.gz    && \
    tar zxvf v2.1-20201229.tar.gz   && \
    tar zxvf v0.3.1.tar.gz          && \
    tar zxvf v0.10.19.tar.gz 
RUN cd luajit2-2.1-20201229         && \
    make -j4                        && \
    make install
ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.1
RUN cd ../nginx-1.19.8              && \
    ./configure  --add-module=../ngx_devel_kit-0.3.1 --add-module=../lua-nginx-module-0.10.19 && \
    make && \
    make install

### ### ###

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf           && \
    touch ../logs/error.log         && \
    chmod +x nginx
COPY nginx.conf /usr/local/nginx/conf/nginx.conf
CMD ["./nginx","-g","daemon off;"]
